﻿Imports System.Math

Module Sub_func_com
    Public Structure stat
        Public Host As String
    End Structure

    Public Structure pia
        Public Nome_operatore As String
        Public Matricola_Piastra As String

    End Structure
    Public Stringa_Errore As String
    Public Salta_trasf_fw As Boolean
    Public Stringa_Salvataggio As String
    Public Label_Salvataggio As String
    Public Piastra As pia
    Public Status As stat
    Public Traspric As String
    Public Stringa_Salvataggio_Pattern_Preset As String = ""
    Public Salva_com As Boolean
    Public Debug_test As Boolean
    Public Debug_pausa As Integer
    Public WorkDir As String
    Public No_Timeout As Boolean = False
    Public Tasto As Integer
    Public k_timeout As Integer
    Public Open_Com As Integer
    'Public Baudrate As Integer
    Public Const PURGE_RXCLEAR As Integer = &H8
    Public WithEvents moRS232 As Rs232
    Private Delegate Sub CommEventUpdate(ByVal source As Rs232, ByVal mask As Rs232.EventMasks)
    Public Stringa_vuota As Boolean
    Public Stringa_di_connessione As String
    Public no_timeout_USB As Boolean
    Public Function stringa_invio(ByVal stringa As String) As String
        stringa_invio = ""
        For a = 0 To Len(stringa) - 1 Step 2

            stringa_invio = stringa_invio + Chr("&h" + stringa.Substring(a, 2))
        Next

    End Function
    Public Function stringa_ricezione(ByVal stringa As String) As String
        Dim aaa As String
        stringa_ricezione = ""
        If stringa <> "" Then
            For a = 1 To Len(stringa)
                aaa = Hex$(Asc(Mid$(stringa, a, 1)))
                If Len(aaa) = 1 Then aaa = "0" + aaa
                stringa_ricezione = stringa_ricezione + aaa
            Next
        End If

    End Function

   
    Public Sub MySleep(ByVal Millisec As Integer)


        If Millisec > 500 Then
            Dim aa As Integer
            aa = Millisec / 10
            For a = 0 To aa
                System.Threading.Thread.Sleep(10)
                Application.DoEvents()
            Next
        Else
            System.Threading.Thread.Sleep(Millisec)
        End If


    End Sub
    Function Stato_Magnetico_MI3G() As String

        Traspsend("D37E54", 2, 50)
        Stato_Magnetico_MI3G = TOGLIE_0(stringa_ricezione(Traspric))

    End Function
    Function mag_checklist() As String
        Traspsend("D37E43", 2, 3000)
        mag_checklist = TOGLIE_0(stringa_ricezione(Traspric))
    End Function

    Function Mag_preset_MI3G()
        Traspsend("D37E50", 2, 3000)
        Mag_preset_MI3G = TOGLIE_0(stringa_ricezione(Traspric))
    End Function
    Function GetVersion() As String
        Traspsend("D37C3F", 64, 1000)
        GetVersion = stringa_invio(TOGLIE_0(stringa_ricezione(Traspric)))
    End Function

    Public Function METTE_0(ByVal pippo As String)
        Dim stringa As String
        stringa = ""

        For a = 1 To Len(pippo)

            stringa = stringa + "0" + Mid$(pippo, a, 1)

        Next

        METTE_0 = stringa

    End Function

    Function dec_hex(ByVal DEC As Byte) As String

        If DEC.ToString <> "" Then

            dec_hex = Hex(DEC) : If Len(dec_hex) = 1 Then dec_hex = "0" & dec_hex
            dec_hex = dec_hex
        Else
            dec_hex = ""

        End If

    End Function
    Function dec_hex(ByVal DEC As Int16) As String


        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 4
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function
    Function dec_hex(ByVal DEC As UInt16) As String


        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 4
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function

    Function dec_hex(ByVal DEC As Int32) As String


        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 8
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function
    Function TOGLIE_0(ByVal pippo As String)
        Dim stringa As String = ""
        For a = 2 To Len(pippo) Step 2

            stringa = stringa + Mid$(pippo, a, 1)

        Next

        TOGLIE_0 = stringa

    End Function


    Public Function hex_bin(ByVal hex As String)
        Dim b As String
        hex_bin = ""
        For a = 1 To Len(hex)

            b = dec_bin("&h" + Mid$(hex, a, 1))
            b = Mid(b, 5)

            hex_bin = hex_bin + b
        Next
        Dim k As Integer
        Select Case hex.Length
            Case 1
                k = 4
            Case 2
                k = 8
            Case 3
                k = 16
            Case 4
                k = 16
        End Select

        For a = 1 To k - Len(hex_bin)

            hex_bin = "0" + Trim(hex_bin)
        Next

    End Function

    Public Function bin_dec(ByVal bin)
        Dim aa As Long = 0
        For a = 0 To Len(bin) - 1
            Application.DoEvents()
            aa = aa + Val(Mid$(bin, Len(bin) - a, 1) * 2 ^ a)
        Next

        bin_dec = aa

    End Function
    Public Function dec_bin(ByVal dec)
        Dim aa As Integer
        On Error Resume Next
        dec_bin = ""
        Do While Not dec = 0
            aa = dec \ 2

            If aa * 2 = Val(dec) Then
                dec_bin = "0" + dec_bin
            Else
                dec_bin = "1" + dec_bin
            End If
            dec = Int(dec / 2)
        Loop

        If dec_bin = "" Then dec_bin = "0"

        For a = 1 To 8 - Len(dec_bin)

            dec_bin = "0" + Trim(dec_bin)
        Next

    End Function

    Public Function int_offset(ByVal st As String) As Integer

        If Mid$(st, 2, 1) = "0" Then
            int_offset = -(Val("&h" + Mid$(st, 3, 2)))
        Else
            int_offset = (Val("&h" + Mid$(st, 3, 2)))
        End If

    End Function

    Function Int_Mag(ByVal stri As String)
        Int_Mag = ""
        For a = 3 To Len(stri) Step 4
            Int_Mag = Int_Mag & Mid$(TOGLIE_0(stringa_ricezione(stri)), a, 2)
        Next

    End Function

    Function Int_Mag_w(ByVal stri As String)
        Int_Mag_w = ""
        For a = 1 To Len(stri) Step 4
            Int_Mag_w = Int_Mag_w + Mid$(TOGLIE_0(stringa_ricezione(stri)), a, 4)
        Next

    End Function

    Function Carattere_pad(ByVal stringa As String, ByVal carattere As Char, ByVal num_tot_caratteri As Byte) As String

        Carattere_pad = stringa

        For a = stringa.Length To num_tot_caratteri - 1
            Carattere_pad = carattere & Carattere_pad
        Next

    End Function
    Public Function bin_hex(ByVal bin)
        Dim p
        bin_hex = ""
        If bin = "0" Then bin_hex = "0" : Exit Function
        Do While Not Len(bin) Mod 4 = 0
            bin = "0" + bin
            Application.DoEvents()
        Loop

        For a = 1 To Len(bin) Step 4
            Application.DoEvents()
            p = Hex(bin_dec(Mid$(bin, a, 4)))
            bin_hex = Trim((bin_hex + p))
        Next

    End Function
    Public Sub Attendi_Invio()
        Dim riapri_seriale As Boolean = False
        If moRS232.IsOpen = True Then
            moRS232.Close()
            riapri_seriale = True
        End If

        Principale.Focus()
        Tasto = 0
        Do While Not Tasto = 13
            Application.DoEvents()
            Threading.Thread.Sleep(50)
        Loop

        If riapri_seriale = True Then
            moRS232.Open()
        End If

    End Sub

    Sub Attendi_Tasto()

        Dim riapri_seriale As Boolean = False
        If moRS232.IsOpen = True Then
            moRS232.Close()
            riapri_seriale = True
        End If

        Principale.Focus()
        Tasto = 0
        Do While Not Tasto <> 0
            Application.DoEvents()
            Threading.Thread.Sleep(50)
        Loop

        If riapri_seriale = True Then
            moRS232.Open()
        End If

    End Sub

    Public Sub Traspsend(ByVal Out As String, ByVal NByte As Integer, ByVal Tout As Integer)
        Dim sTx As String
        Traspric = ""
        '----------------------
        '// Clear Tx/Rx Buffers
        moRS232.PurgeBuffer(Rs232.PurgeBuffers.TxClear Or Rs232.PurgeBuffers.RXClear)
        moRS232.RxBufferThreshold = Int32.Parse(NByte)
        moRS232.Timeout = Val(Tout)
        sTx = stringa_invio(Out)
        moRS232.Write(sTx)
        'moRS232.Write(Chr(2) & Chr(2) & Chr(73) & Chr(48) & Chr(121) & Chr(3))
        '// Clears Rx textbox
        If NByte > 0 Then
            Ricezione(NByte)
        Else
            MySleep(Tout)
        End If
    End Sub

    Private Sub Ricezione(ByVal nbyte As Integer)

        moRS232.Read(Int32.Parse(nbyte))
        Traspric = moRS232.InputStreamString.ToString
        If Stringa_vuota = True Then Traspric = ""
        '
    End Sub
    Private Sub moRS232_CommEvent(ByVal source As Rs232, ByVal Mask As Rs232.EventMasks) Handles moRS232.CommEvent
        '===================================================
        '												©2003 www.codeworks.it All rights reserved
        '
        '	Description	:	Events raised when a comunication event occurs
        '	Created			:	15/07/03 - 15:13:46
        '	Author			:	Corrado Cavalli
        '
        '						*Parameters Info*
        '
        '	Notes				:	
        '===================================================
        Debug.Assert(Principale.InvokeRequired = False)

        Dim iPnt As Int32, Buffer() As Byte
        Debug.Assert(Principale.InvokeRequired = False)

        If (Mask And Rs232.EventMasks.RxChar) > 0 Then

            Buffer = source.InputStream
            For iPnt = 0 To Buffer.Length - 1

            Next

        End If

    End Sub

#Region "UI update routine"
#End Region

    Public Sub Mem_Dump(ByVal DevId As Byte, ByVal BlockAdr As String, ByVal BlockSize As String)

        BlockAdr = METTE_0(BlockAdr)
        Dim BlockSizes As String = METTE_0(BlockSize)
        Dim aa As String
        aa = "D3330" & Hex(DevId) & BlockAdr & BlockSizes
        Traspsend(aa, (CLng("&h" & BlockSize) * 2), 20000)
    End Sub

    Public Sub Mem_Write(ByVal DevId As Byte, ByVal BlockAdr As String, ByVal BlockSize As String, ByVal DataStream As String)

        BlockAdr = METTE_0(BlockAdr)
        BlockSize = METTE_0(BlockSize)
        DataStream = METTE_0(DataStream)
        Dim aa As String
        aa = "D3331" & DevId.ToString & BlockAdr & BlockSize & DataStream

        Traspsend(aa, 0, 100)
    End Sub
    Public Sub RS32SptSet(ByVal indirizzoH As Int16, ByVal indirizzoL As Int16, ByVal datoH As Byte, ByVal datoL As Byte)

        'RS232 cmd0xD3 0x70 0x20
        'Parms SptAddress,SptData
        'Answer status
        'T/Out max 500 ms
        'Scrittura di una locazione di Spt (E2prom):
        'o SptAddress: indirizzo nella Spt nel f.to 0d 0d 0d 0d;
        'o SptData: dato da scrivere nella locazione nel f.to 0d 0d


        Dim ind As String = METTE_0(dec_hex(indirizzoH))
        Dim dat As String = METTE_0(dec_hex(datoH))
        Traspsend("D37020" & ind & dat, 1, 500)
        ind = METTE_0(Carattere_pad(dec_hex(indirizzoL), "0", 4))
        dat = METTE_0(Carattere_pad(dec_hex(datoL), "0", 2))
        Traspsend("D37020" & ind & dat, 1, 500)
    End Sub

    Sub RS32Adjust(ByVal src As String, ByVal saveflag As Boolean)
        'cmd()0xD3 0x07 0x30
        'Parms(Src, SaveFlag)
        'Answer()--
        'T/Out max 6sec()

        '0x00-0x04 Sorgenti ottiche a seconda della configurazione
        '0x05-0x0F Reserved()
        'x10(DARK())
        'x11(AFE(Offset))
        Dim SF As String
        If saveflag = True Then
            SF = "01"
        Else
            SF = "00"
        End If

        Call Traspsend("D30730" & src & SF, 0, 0)

        attendi_per_t("41", 200, 6000)


    End Sub


    Public Sub RS32SetPga(ByVal Front_rear As Byte, ByVal pga0 As Int16, ByVal pga1 As Int16, ByVal pga2 As Int16, ByVal salva_spt As Boolean)
        'Command 0xD3 0x07 0x24
        'Parms()
        'RefAFE()
        'Pga_0()Pga_1()Pga_2()SptSaveFlag()
        'Answer()--
        'T/Out max 50 ms()

        ' RefAFE: (1 byte) AFE di riferimento 0=Front 1=Rear
        ' Pga_n: (4 byte) valore del PGA che si vuole impostare in formato word splitted 0d 0d 0d 0d
        ' SptSaveFlag: (1 byte) flag per il salvataggio contemporaneo in Spt 0=non salva 1=salva

        Dim pga_0, pga_1, pga_2, s_spt As String

        pga_0 = METTE_0(Carattere_pad(dec_hex(pga0), "0", 4))
        pga_1 = METTE_0(Carattere_pad(dec_hex(pga1), "0", 4))
        pga_2 = METTE_0(Carattere_pad(dec_hex(pga2), "0", 4))
        s_spt = METTE_0(Abs(Val(salva_spt)))
        Traspsend("D30724" & METTE_0(Front_rear) & pga_0 & pga_1 & pga_2 & s_spt, 0, 0)
        attendi_per_t("41", 100, 500)

    End Sub
    Sub RS32SetMode(ByVal TrDirection As Boolean, ByVal SequenceMode As Boolean, ByVal ResolutionMode As Boolean, ByVal Rsvd As Boolean, ByVal ModelMask As Boolean, ByVal NoteProcEvtMode As Boolean, ByVal AdjDisableMask As Boolean, ByVal DarkSubFlag As Boolean, ByVal CompFlag As Boolean, ByVal ClockFlag As Boolean, ByVal LightEnaFlag As Boolean, ByVal ProcessingFlag As Boolean, ByVal FixedLenFlag As Boolean, ByVal TrackingCIS As Boolean, ByVal CfxPtr As Boolean, ByVal AdjCycleFlag As Boolean)

        Dim stri As String = Abs(Val(TrDirection)) & Abs(Val(SequenceMode)) & Abs(Val(ResolutionMode)) & Abs(Val(Rsvd)) & Abs(Val(ModelMask)) & Abs(Val(NoteProcEvtMode)) & Abs(Val(AdjDisableMask)) & Abs(Val(DarkSubFlag)) & Abs(Val(CompFlag)) & Abs(Val(ClockFlag)) & Abs(Val(LightEnaFlag)) & Abs(Val(ProcessingFlag)) & Abs(Val(FixedLenFlag)) & Abs(Val(TrackingCIS)) & Abs(Val(CfxPtr)) & Abs(Val(AdjCycleFlag))

        Traspsend("D32301" & METTE_0(stri), 0, 0)

        attendi_per_t("41", 100, 1000)

        'Sensor.GetModuleDesc()

    End Sub

    Function attendi_passaggio_banconota(ByVal timeout As Integer) As Boolean

        attendi_passaggio_banconota = False
        No_Timeout = True
        Traspsend("", 3, timeout)
        No_Timeout = False
        If Traspric.Length = 3 Then
            attendi_passaggio_banconota = True
        End If

    End Function

    Sub PWMTarSet()

        Traspsend("D33200000206", 0, 0)
        attendi_per_t("41", 200, 6000)

    End Sub

    Function Controllo_Errore_DSP_OPT(ByVal Invia_F3 As Boolean) As Boolean

        If Invia_F3 = True Then Traspsend("F3", 1, 200)

        Select Case stringa_ricezione(Traspric)
            Case "07", "08", "16", "18", "22", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5A", "5B", "5C", "5D", "5E", "5F"
                Controllo_Errore_DSP_OPT = False
            Case Else
                Controllo_Errore_DSP_OPT = True
        End Select

    End Function



    Public Function Connessione_Automatica(Optional statusCmd As String = "F9") As Byte
        Dim aakk As Integer

        For a = 1 To 18
            moRS232.Port = a
            Try
                open_port(a)
            Catch
                GoTo fine
            End Try

            For b = 0 To 4
                moRS232.Close()
                Select Case b
                    Case 0
                        moRS232.BaudRate = 9600

                    Case 1
                        moRS232.BaudRate = 19200

                    Case 2
                        moRS232.BaudRate = 38400

                    Case 3
                        moRS232.BaudRate = 57600

                    Case 4
                        moRS232.BaudRate = 115200

                End Select

                moRS232.Open()



ss:
                'moRS232.PurgeBuffer(PURGE_RXCLEAR)

                Traspric = ""
                Call Traspsend(statusCmd, 0, 20)
                Dim Stato As String = ""
                If moRS232.InBufferCount = 1 Then
                    moRS232.Read(1)
                    Stato = stringa_ricezione(moRS232.InputStreamString)
                End If

                aakk = moRS232.BaudRate
                Dim aa As String
                If Val("&h" & Stato) > 127 Then
                    Exit For
                End If
                If Stato <> "" Then
                    Select Case Stato

                        Case "00"

                        Case "41"
                            aa = attendi_per_t("41", 100, 6000)
                            If aa = False Then
                                Stringa_di_connessione = "Lettore in stato Busy, impossibile continuare, premere un tasto"
                                Connessione_Automatica = 0
                                Exit Function
                            End If
                            If aa = True Then GoTo ss
                        Case Else
                            aa = moRS232.BaudRate
                            For c = 1 To 8
                                MySleep(10)
                                Call Traspsend(statusCmd, 1, 100)
                                If Stato <> stringa_ricezione(Traspric) Then
                                    Exit For
                                End If
                            Next

                            Connessione_Automatica = 16
                            Exit Function

                    End Select
                End If
            Next
fine:
        Next
salta:

        Connessione_Automatica = 0
        Stringa_di_connessione = "Impossibile connettersi al modulo in prova" & Chr(13) & "Verificare i collegamenti e l'alimentazione del lettore" & Chr(13) & "Premere un tasto per ripetere, ESC per uscire"

    End Function
    Sub attendi_per(ByVal stato As String)
        Traspric = Chr("&H" + stato)
        Do While Not Traspric <> Chr("&H" + stato)
            Call Traspsend("F3", 1, 1000)
            If Mid$(Traspric, 1, 1) = Chr(0) Then
                Traspric = Chr("&H" + stato)
            End If
            MySleep(100)

        Loop

    End Sub
    Function attendi_per_t(ByVal stato As String, ByVal lop As Long, ByVal t As Integer) As Boolean

        Application.DoEvents()

        Traspric = Chr("&H" & stato)
        Dim limite As DateTime = DateTime.Now.AddMilliseconds(t)
        Do While Not Traspric <> Chr("&H" & stato)
            If DateTime.Now > limite Then
                attendi_per_t = False

                Exit Function
            End If
            Call Traspsend("F3", 1, 1000)
            If Mid$(Traspric, 1, 1000) = Chr(0) Then
                Traspric = Chr("&H" & stato)
            End If
            MySleep(lop)
            Application.DoEvents()
        Loop
        If Traspric = "" Then attendi_per_t = False : Exit Function

        attendi_per_t = True


    End Function
    Function Cambio_Velocita_Seriale(ByVal velocita As Integer, ByVal invio As Boolean)
        Dim caga As String
        Traspric = ""
        'attendi_per("41")

        If invio = True Then
            If velocita = 4 Then
                Call Traspsend("D37700", 0, 0)
            Else
                Call Traspsend("D3470" + Trim(Str(velocita)), 0, 0)
            End If

        End If
        Call MySleep(1000)
        Select Case velocita
            Case 0
                moRS232.BaudRate = 9600

            Case 1
                moRS232.BaudRate = 19200

            Case 2
                moRS232.BaudRate = 38400

            Case 3
                moRS232.BaudRate = 57600

            Case 4
                moRS232.BaudRate = 115200
        End Select
        'If invio = True Then
        '    If velocita <> 4 Then
        '        Call ricezione("", 1, 6000)
        '    End If
        'End If
        Call MySleep(1000)
        moRS232.Close()
        moRS232.Open()
        If moRS232.InBufferCount > 0 Then
            moRS232.Read(moRS232.InBufferCount)
            caga = moRS232.InputStreamString
        End If
        caga = moRS232.BaudRate
        attendi_per_t("41", 500, 2000)

        If Traspric <> "" Then Cambio_Velocita_Seriale = True Else Cambio_Velocita_Seriale = False

    End Function
    Public Function Ext_Trig() As Boolean
        Dim aa As String
        If Read_Set("Encoder", "hw_level") <> "Failed" Then
            Ext_Trig = False
            Call Traspsend("D355", 1, 5000)
            aa = stringa_ricezione(Traspric)
            If aa = "00" Then
                Call Traspsend("D35401", 1, 3000)
                Ext_Trig = True
            End If
        Else
            Ext_Trig = True
        End If

    End Function
    Public Sub Int_Trig()
        Dim aa As String
        Call Traspsend("D355", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call Traspsend("D35400", 1, 3000)
        End If

    End Sub
    Public Sub Auto_Answ_ON()
        Dim aa As String
        Call Traspsend("D353", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Call Traspsend("D35201", 1, 3000)
        End If
    End Sub

    Public Sub Auto_Answ_OFF()
        Dim aa As String
        Call Traspsend("D353", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call Traspsend("D35200", 0, 0)
            attendi_per("41")
        End If
    End Sub

    Public Sub Dbl_On()
        Dim aa As String
        Call Traspsend("D349", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Call Traspsend("D34801", 1, 3000)
        End If
    End Sub

    Public Sub Dbl_Off()
        Dim aa As String
        Call Traspsend("D349", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call Traspsend("D34800", 1, 3000)
        End If
    End Sub

    'Public Function Reboot(Optional ByVal cambiare_velocità As Boolean = False, Optional ByVal velocità_seriale As Integer = 0) As Boolean
    '    'Dim usb_ric As String

    '    Call Traspsend("D37D", 1, 5000)
    '    MySleep(3000)
    '    If cambiare_velocità = True Then
    '        Cambio_Velocita_Seriale(velocità_seriale, False)
    '    End If
    '    Dim usb_ok As Boolean = False
    '    If attendi_per_t("41", 300, 5000) = False Then
    '        Throw New IOTimeoutException("Lettore in Busy dopo reboot, impossibile continuare")
    '    End If
    '    Call MySleep(50)
    '    Call Traspsend("D31300", 0, 0)

    '    no_timeout_USB = True
    '    For a = 1 To 3
    '        Dim usb_ric As String = USB_Array_to_String(USBB("20", 12, 1000, False))
    '        If Mid$(usb_ric, 1, 12) = "123456 prova" Then
    '            usb_ok = True
    '            Exit For
    '        End If
    '        Call MySleep(2000)
    '    Next
    '    no_timeout_USB = False
    '    Reboot = usb_ok

    'End Function
    Public Function Base64Encode(ByVal in_str As String) As String
        Const Base64$ = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
        Dim sOut As String, i As Long, str_len As Long
        Dim nGroup, pOut As String, int_a As Byte, int_b As Byte, int_c As Byte, s As String

        str_len = Len(in_str)
        sOut = ""
        For i = 1 To str_len Step 3
            s = Mid$(in_str, i + 0, 1) : int_a = Asc(s)
            s = Mid$(in_str, i + 1, 1) : If (s = "") Then int_b = 0 Else int_b = Asc(s)
            s = Mid$(in_str, i + 2, 1) : If (s = "") Then int_c = 0 Else int_c = Asc(s)
            nGroup = &H10000 * CLng(int_a) + &H100 * CLng(int_b) + CLng(int_c)
            nGroup = Oct(nGroup)
            nGroup = StrDup(8 - Len(nGroup), "0") & nGroup
            pOut = Mid$(Base64, CLng("&o" & Mid$(nGroup, 1, 2)) + 1, 1) + _
                                    Mid$(Base64, CLng("&o" & Mid$(nGroup, 3, 2)) + 1, 1) + _
                                    Mid$(Base64, CLng("&o" & Mid$(nGroup, 5, 2)) + 1, 1) + _
                                    Mid$(Base64, CLng("&o" & Mid$(nGroup, 7, 2)) + 1, 1)
            sOut = sOut + pOut
        Next
        Select Case (str_len Mod 3)
            Case 1 '8 bit final
                sOut = Left$(sOut, Len(sOut) - 2) & "=="
            Case 2 '16 bit final
                sOut = Left$(sOut, Len(sOut) - 1) & "="
        End Select
        ' Trasformo la stringa secondo i requisiti (cr + lf + 72 crt + cr + lf)
        Base64Encode = vbCr & vbLf
        Do
            str_len = Len(sOut)
            If (str_len > 72) Then str_len = 72
            Base64Encode = Base64Encode & Mid$(sOut, 1, str_len) & vbCr & vbLf
            sOut = Right$(sOut, Len(sOut) - str_len)
            If (Len(sOut) = 0) Then Exit Do
        Loop
    End Function

    Public Sub open_port(ByVal com As Integer)
        moRS232 = New Rs232()

        '// Setup parameters
        With moRS232
            .Port = Round(com)
            .BaudRate = Int32.Parse(9600)
            .DataBit = 8
            .StopBit = Rs232.DataStopBit.StopBit_1
            .Parity = Rs232.DataParity.Parity_None
            .Timeout = Int32.Parse(1000)
        End With
        '// Initializes port
        moRS232.Open()

        'If chkEvents.Checked Then moRS232.EnableEvents()
        'chkEvents.Enabled = True

    End Sub

    Private Sub close_port()

        moRS232.Close()

    End Sub

    Function USB_Array_to_String(ByVal buf As Byte()) As String
        USB_Array_to_String = ""
        If buf.Length > 0 Then
            For a = 0 To buf.Length - 1
                USB_Array_to_String = USB_Array_to_String & Chr(buf(a))
            Next
        End If
    End Function

    Function ReadSensorStream(ByVal DeviceID As Byte, ByVal SensorID As Byte, ByVal StreamID As Byte, ByVal BufferPtr As Byte, ByVal nbyte As Int32) As Byte()
        'USB(cmd 0x92 0x02
        'Parms(DeviceID, SensorID, StreamID, BufferPtr)
        'Answer()[DataPrecision x ImageW x ImageH] bytes
        'T/Out max 5:      MS()
        'BufferPtr indica la banconota (presente nel buffer circolare) per la quale deve essere restituito lo stream (varia tra 0 e BufferDepth-1). Il valore 0xFF indica l’ultima banconota acquisita. Le operazioni in single buffer prevedono l’uso del solo valore 0.
        Dim kk As String = "9202" & dec_hex(DeviceID) & dec_hex(SensorID) & dec_hex(StreamID) & dec_hex(BufferPtr)

        ReadSensorStream = USBB(kk, nbyte, 1000, False)


    End Function

    Function Mette_4_0(ByVal val As String)
        Mette_4_0 = val
        For a = 1 To 4 - val.Length
            Mette_4_0 = "0" & Mette_4_0
        Next
    End Function

    Function Mette_2_0(ByVal val As String)
        Mette_2_0 = val
        For a = 1 To 2 - val.Length
            Mette_2_0 = "0" & Mette_2_0
        Next
    End Function
    Sub SetPresetRef()

        Traspsend("D30722", 0, 0)
        attendi_per("41")

    End Sub

End Module

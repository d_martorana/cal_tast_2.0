!define APPNAME "Cal Tast"
!define COMPANYNAME "ARCA"
!define VERSIONMAJOR 2
!define VERSIONMINOR 0
!define VERSIONBUILD 2
!define INSTFOLDER "C:\Arca\CalTast2"
!define SOURCEFOLDER "D:\Progetti VBNET\Cal_Tast_2.0\Cal_Tast_2.0\bin\Debug"
OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
;RequestExecutionLevel none/admin/user/highest



Section "install"
	;Banner::show /set 76 "Banner Visualizzato" /set 54 "Normal Text"
	SetOutPath "${INSTFOLDER}"
	File "${SOURCEFOLDER}\Cal_Tast.ini"
	File "${SOURCEFOLDER}\Cal_Tast_2.0.exe"
	File "${SOURCEFOLDER}\Cal_Test_Setup.ini"
	File "${SOURCEFOLDER}\T2-TAST.ini"
	File "${SOURCEFOLDER}\ZedGraph.dll"
	File "${SOURCEFOLDER}\Interop.cartesio.dll"
	File "${SOURCEFOLDER}\Interop.VBRUN.dll"
	File "${SOURCEFOLDER}\libusb-1.0.DLL"
	File "${SOURCEFOLDER}\OEMR_USB.DLL"
	
	;RegDLL "${INSTFOLDER}\libusb-1.0.DLL"
	;RegDLL "${INSTFOLDER}\OEMR_USB.DLL"
	
	CreateShortCut "$DESKTOP\${APPNAME}.lnk" "${INSTFOLDER}\Cal_Tast_2.0.exe" ""
	
	WriteUninstaller "${INSTFOLDER}\Uninstall.exe"
SectionEnd

Section "Uninstall"
	RMDir /r "${INSTFOLDER}\*.*"
	RMDir "${INSTFOLDER}"
	Delete "$DESKTOP\${APPNAME}.lnk"
SectionEnd

Function .onInstSuccess
	#MessageBox MB_OK "Installazione eseguita correttamente!"
FunctionEnd

Function un.onUninstSuccess
	#MessageBox MB_OK "Disinstallazione eseguita correttamente!"
FunctionEnd
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principale
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principale))
        Dim DataGridViewCellStyle181 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle182 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle183 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle184 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle185 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle186 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle187 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle188 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle189 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle190 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle191 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle192 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle193 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle194 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle195 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle196 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle197 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle198 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Text1 = New System.Windows.Forms.TextBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Graph1 = New ZedGraph.ZedGraphControl()
        Me.Graph2 = New ZedGraph.ZedGraphControl()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Grid6 = New System.Windows.Forms.DataGridView()
        Me.Grid5 = New System.Windows.Forms.DataGridView()
        Me.Grid3 = New System.Windows.Forms.DataGridView()
        Me.Grid4 = New System.Windows.Forms.DataGridView()
        Me.Grid2 = New System.Windows.Forms.DataGridView()
        Me.Grid1 = New System.Windows.Forms.DataGridView()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(30, 170)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 17
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(732, 170)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(86, 25)
        Me.Button2.TabIndex = 16
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'Text1
        '
        Me.Text1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text1.Location = New System.Drawing.Point(314, 228)
        Me.Text1.MaxLength = 16
        Me.Text1.Name = "Text1"
        Me.Text1.Size = New System.Drawing.Size(259, 29)
        Me.Text1.TabIndex = 15
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(70, 259)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(661, 23)
        Me.ProgressBar1.TabIndex = 14
        Me.ProgressBar1.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 200.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(264, 300)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(718, 299)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "FAIL"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label2.Visible = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(314, 204)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(229, 61)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Avvia Test"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Graph1
        '
        Me.Graph1.IsEnableHPan = False
        Me.Graph1.IsEnableHZoom = False
        Me.Graph1.IsEnableVEdit = True
        Me.Graph1.IsEnableVPan = False
        Me.Graph1.IsEnableVZoom = False
        Me.Graph1.IsEnableWheelZoom = False
        Me.Graph1.IsPrintFillPage = False
        Me.Graph1.IsPrintKeepAspectRatio = False
        Me.Graph1.IsPrintScaleAll = False
        Me.Graph1.Location = New System.Drawing.Point(22, 288)
        Me.Graph1.Name = "Graph1"
        Me.Graph1.ScrollGrace = 0.0R
        Me.Graph1.ScrollMaxX = 0.0R
        Me.Graph1.ScrollMaxY = 0.0R
        Me.Graph1.ScrollMaxY2 = 0.0R
        Me.Graph1.ScrollMinX = 0.0R
        Me.Graph1.ScrollMinY = 0.0R
        Me.Graph1.ScrollMinY2 = 0.0R
        Me.Graph1.Size = New System.Drawing.Size(215, 289)
        Me.Graph1.TabIndex = 18
        Me.Graph1.Visible = False
        Me.Graph1.ZoomButtons = System.Windows.Forms.MouseButtons.None
        Me.Graph1.ZoomStepFraction = 1.0R
        '
        'Graph2
        '
        Me.Graph2.IsEnableHPan = False
        Me.Graph2.IsEnableHZoom = False
        Me.Graph2.IsEnableVPan = False
        Me.Graph2.IsEnableVZoom = False
        Me.Graph2.IsEnableWheelZoom = False
        Me.Graph2.IsPrintFillPage = False
        Me.Graph2.IsPrintKeepAspectRatio = False
        Me.Graph2.Location = New System.Drawing.Point(154, 288)
        Me.Graph2.Name = "Graph2"
        Me.Graph2.ScrollGrace = 0.0R
        Me.Graph2.ScrollMaxX = 0.0R
        Me.Graph2.ScrollMaxY = 0.0R
        Me.Graph2.ScrollMaxY2 = 0.0R
        Me.Graph2.ScrollMinX = 0.0R
        Me.Graph2.ScrollMinY = 0.0R
        Me.Graph2.ScrollMinY2 = 0.0R
        Me.Graph2.Size = New System.Drawing.Size(194, 155)
        Me.Graph2.TabIndex = 19
        Me.Graph2.Visible = False
        Me.Graph2.ZoomStepFraction = 1.0R
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(408, 244)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 20
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(51, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(321, 57)
        Me.Label1.TabIndex = 21
        '
        'Grid6
        '
        DataGridViewCellStyle181.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle181.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle181.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle181.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle181.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle181.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle181.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid6.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle181
        Me.Grid6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid6.ColumnHeadersVisible = False
        DataGridViewCellStyle182.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle182.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle182.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle182.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle182.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle182.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle182.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid6.DefaultCellStyle = DataGridViewCellStyle182
        Me.Grid6.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Grid6.Enabled = False
        Me.Grid6.Location = New System.Drawing.Point(68, 187)
        Me.Grid6.Name = "Grid6"
        Me.Grid6.ReadOnly = True
        DataGridViewCellStyle183.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle183.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle183.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle183.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle183.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle183.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle183.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid6.RowHeadersDefaultCellStyle = DataGridViewCellStyle183
        Me.Grid6.RowHeadersVisible = False
        Me.Grid6.Size = New System.Drawing.Size(240, 150)
        Me.Grid6.TabIndex = 27
        Me.Grid6.Visible = False
        '
        'Grid5
        '
        DataGridViewCellStyle184.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle184.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle184.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle184.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle184.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle184.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle184.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid5.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle184
        Me.Grid5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid5.ColumnHeadersVisible = False
        DataGridViewCellStyle185.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle185.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle185.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle185.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle185.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle185.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle185.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid5.DefaultCellStyle = DataGridViewCellStyle185
        Me.Grid5.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Grid5.Enabled = False
        Me.Grid5.Location = New System.Drawing.Point(479, 187)
        Me.Grid5.Name = "Grid5"
        Me.Grid5.ReadOnly = True
        DataGridViewCellStyle186.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle186.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle186.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle186.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle186.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle186.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle186.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid5.RowHeadersDefaultCellStyle = DataGridViewCellStyle186
        Me.Grid5.RowHeadersVisible = False
        Me.Grid5.Size = New System.Drawing.Size(240, 150)
        Me.Grid5.TabIndex = 26
        Me.Grid5.Visible = False
        '
        'Grid3
        '
        DataGridViewCellStyle187.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle187.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle187.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle187.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle187.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle187.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle187.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid3.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle187
        Me.Grid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid3.ColumnHeadersVisible = False
        DataGridViewCellStyle188.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle188.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle188.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle188.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle188.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle188.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle188.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid3.DefaultCellStyle = DataGridViewCellStyle188
        Me.Grid3.Enabled = False
        Me.Grid3.Location = New System.Drawing.Point(52, 159)
        Me.Grid3.Name = "Grid3"
        Me.Grid3.ReadOnly = True
        DataGridViewCellStyle189.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle189.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle189.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle189.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle189.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle189.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle189.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid3.RowHeadersDefaultCellStyle = DataGridViewCellStyle189
        Me.Grid3.RowHeadersVisible = False
        Me.Grid3.Size = New System.Drawing.Size(213, 150)
        Me.Grid3.TabIndex = 25
        Me.Grid3.Visible = False
        '
        'Grid4
        '
        DataGridViewCellStyle190.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle190.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle190.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle190.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle190.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle190.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle190.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid4.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle190
        Me.Grid4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid4.ColumnHeadersVisible = False
        DataGridViewCellStyle191.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle191.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle191.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle191.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle191.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle191.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle191.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid4.DefaultCellStyle = DataGridViewCellStyle191
        Me.Grid4.Enabled = False
        Me.Grid4.Location = New System.Drawing.Point(34, 143)
        Me.Grid4.Name = "Grid4"
        Me.Grid4.ReadOnly = True
        DataGridViewCellStyle192.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle192.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle192.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle192.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle192.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle192.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle192.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid4.RowHeadersDefaultCellStyle = DataGridViewCellStyle192
        Me.Grid4.RowHeadersVisible = False
        Me.Grid4.Size = New System.Drawing.Size(183, 159)
        Me.Grid4.TabIndex = 24
        Me.Grid4.Visible = False
        '
        'Grid2
        '
        DataGridViewCellStyle193.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle193.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle193.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle193.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle193.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle193.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle193.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle193
        Me.Grid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid2.ColumnHeadersVisible = False
        DataGridViewCellStyle194.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle194.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle194.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle194.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle194.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle194.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle194.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid2.DefaultCellStyle = DataGridViewCellStyle194
        Me.Grid2.Enabled = False
        Me.Grid2.Location = New System.Drawing.Point(473, 176)
        Me.Grid2.Name = "Grid2"
        Me.Grid2.ReadOnly = True
        DataGridViewCellStyle195.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle195.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle195.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle195.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle195.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle195.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle195.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid2.RowHeadersDefaultCellStyle = DataGridViewCellStyle195
        Me.Grid2.RowHeadersVisible = False
        Me.Grid2.Size = New System.Drawing.Size(207, 151)
        Me.Grid2.TabIndex = 23
        Me.Grid2.Visible = False
        '
        'Grid1
        '
        DataGridViewCellStyle196.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle196.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle196.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle196.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle196.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle196.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle196.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle196
        Me.Grid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid1.ColumnHeadersVisible = False
        DataGridViewCellStyle197.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle197.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle197.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle197.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle197.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle197.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle197.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid1.DefaultCellStyle = DataGridViewCellStyle197
        Me.Grid1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Grid1.Enabled = False
        Me.Grid1.Location = New System.Drawing.Point(574, 228)
        Me.Grid1.Name = "Grid1"
        Me.Grid1.ReadOnly = True
        DataGridViewCellStyle198.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle198.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle198.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle198.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle198.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle198.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle198.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid1.RowHeadersDefaultCellStyle = DataGridViewCellStyle198
        Me.Grid1.RowHeadersVisible = False
        Me.Grid1.Size = New System.Drawing.Size(240, 150)
        Me.Grid1.TabIndex = 22
        Me.Grid1.Visible = False
        '
        'Principale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1268, 520)
        Me.Controls.Add(Me.Text1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Grid6)
        Me.Controls.Add(Me.Grid5)
        Me.Controls.Add(Me.Grid3)
        Me.Controls.Add(Me.Grid4)
        Me.Controls.Add(Me.Grid2)
        Me.Controls.Add(Me.Grid1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Graph2)
        Me.Controls.Add(Me.Graph1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Principale"
        Me.Text = "Cal_tast"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Text1 As System.Windows.Forms.TextBox
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Graph1 As ZedGraph.ZedGraphControl
    Friend WithEvents Graph2 As ZedGraph.ZedGraphControl
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Grid6 As System.Windows.Forms.DataGridView
    Friend WithEvents Grid5 As System.Windows.Forms.DataGridView
    Friend WithEvents Grid3 As System.Windows.Forms.DataGridView
    Friend WithEvents Grid4 As System.Windows.Forms.DataGridView
    Friend WithEvents Grid2 As System.Windows.Forms.DataGridView
    Friend WithEvents Grid1 As System.Windows.Forms.DataGridView

End Class

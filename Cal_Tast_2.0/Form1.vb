﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports System.Math
Imports ZedGraph
Public Class Principale



    Public Test As Boolean

    Public errore_offset As String
    Public Errore_acquisizione As String
    Dim zero_elettrico As Byte

    Private Sub Principale_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Application.Exit()
    End Sub

    Private Sub Principale_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Application.Exit()
    End Sub

    Private Sub Principale_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Tasto = e.KeyCode
    End Sub

    Private Sub Principale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Label1.Top = Screen.PrimaryScreen.Bounds.Height / 15
            Label1.Width = Screen.PrimaryScreen.Bounds.Width - (Label1.Top * 2)
            Label1.Left = (Screen.PrimaryScreen.Bounds.Width - Label1.Width) / 2
            Label1.Height = Screen.PrimaryScreen.Bounds.Height / 7

            Text1.Top = Label1.Top + Label1.Height + 50

            Text1.Left = (Screen.PrimaryScreen.Bounds.Width - Text1.Width) / 2


            Button1.Width = Screen.PrimaryScreen.Bounds.Width / 6
            Button1.Height = Screen.PrimaryScreen.Bounds.Height / 15
            Button1.Left = (Screen.PrimaryScreen.Bounds.Width - Button1.Width) / 2
            Button1.Top = (Screen.PrimaryScreen.Bounds.Height) / 2

            Label2.Left = (Screen.PrimaryScreen.Bounds.Width - Label2.Width) / 2
            Label2.Top = (Screen.PrimaryScreen.Bounds.Height - Label2.Height) / 2

            'PictureBox1.Width = Screen.PrimaryScreen.Bounds.Width / 6
            PictureBox1.Left = (Screen.PrimaryScreen.Bounds.Width - PictureBox1.Width) / 2
            PictureBox1.Top = Label1.Top + Label1.Height + PictureBox1.Height

            Graph1.Width = 540
            Graph1.Height = 365
            Graph2.Width = 540
            Graph2.Height = 365

            ProgressBar1.Width = Label1.Width
            ProgressBar1.Top = Button1.Top
            ProgressBar1.Left = Label1.Left


            WorkDir = Application.StartupPath & "\"
            Dim com_port As String = Read_Set("Comunicazione", "Com")
            k_timeout = Read_Set("Comunicazione", "K_timeout")
            Me.Text = "Collaudo e calibrazione modulo tastatore"
            'SerialPort1.PortName = "COM" & com_port
            Open_Com = (com_port)
            Debug_test = Read_Set("Debug", "Attendi_tasto")
            Debug_pausa = Read_Set("Debug", "Pausa")
            'Salta_DWL = Read_Set("Debug", "Salta_DWL")
            'Salta_trasf_fw = Read_Set("Debug", "Salta_trasf_fw ")
            Text1.Visible = False
        Catch ex As IO.FileLoadException
            MessageBox.Show(ex.Message, "Errore File INI")
            Application.Exit()
            End
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Errore")
            Application.Exit()
            End
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim aa
            Label2.Text = "FAIL"
            Label2.ForeColor = Color.Red

            Test = False
            Button1.Visible = False
            Button1.Enabled = False
            Dim msg As String
            Stringa_Salvataggio = ""
            Stringa_Salvataggio_Pattern_Preset = ""
            moRS232 = New Rs232

inserisci_operatore:

            Text1.Text = ""

            If Piastra.Nome_operatore = "" Then
                Text1.Visible = True
                Text1.Focus()
                Scrivi("Inserire nome operatore e premere Invio", Color.Blue)
                Attendi_Invio()
                If Text1.Text.Length < 2 Then
                    msg = "Il nome operatore deve essere di almeno 2 caratteri, introdurlo nuovamente"  ' Define message.
                    MsgBox(msg, MsgBoxStyle.Critical)
                    GoTo Inserisci_operatore
                End If
                Piastra.Nome_operatore = Text1.Text

            End If

Inserisci_matricola:
            Text1.Visible = True
            Text1.Focus()
            Text1.Text = ""
            Text1.Focus()
            Scrivi("Inserire matricola piastra e premere Invio", Color.Blue)
            Attendi_Invio()

            Text1.Text = Text1.Text.ToUpper

            If Len(Text1.Text) <> 16 Then
                msg = "La matricola lettore deve obbligatoriamente essere composta di 16 caratteri, introdurla nuovamente"  ' Define message.
                MsgBox(msg, MsgBoxStyle.Critical)
                GoTo Inserisci_matricola
            End If

            If Asc(Mid$(Text1.Text, 1, 1)) < 65 Or Asc(Mid$(Text1.Text, 1, 1)) > 90 Then

                msg = "La matricola presenta caratteri non validi, introdurla nuovamente"  ' Define message.
                MsgBox(msg, MsgBoxStyle.Critical)
                GoTo Inserisci_matricola

            End If
            Dim kkk As Integer
            For a = 2 To 11
                kkk = Asc(Mid$(Text1.Text, a, 1))
                If kkk < 48 Or kkk > 57 Then

                    msg = "La matricola presenta caratteri non validi, introdurla nuovamente"  ' Define message.
                    MsgBox(msg, MsgBoxStyle.Critical)
                    GoTo Inserisci_matricola
                End If
            Next

            aa = Val(Mid$(Text1.Text, 9, 2))

            If Val(Mid$(Text1.Text, 9, 2)) < 1 Or Val(Mid$(Text1.Text, 9, 2)) > 52 Then

                msg = "La matricola presenta caratteri non validi, introdurla nuovamente"  ' Define message.
                MsgBox(msg, MsgBoxStyle.Critical)
                GoTo Inserisci_matricola
            End If

            For a = 12 To 16
                kkk = Asc(Mid$(Text1.Text, a, 1))
                If kkk < 48 Or kkk > 57 And kkk < 65 Or kkk > 70 Then

                    msg = "La matricola presenta caratteri non validi, introdurla nuovamente"  ' Define message.
                    MsgBox(msg, MsgBoxStyle.Critical)
                    GoTo Inserisci_matricola
                End If
            Next

            Piastra.Matricola_Piastra = Text1.Text
            Me.Text = "Collaudo e calibrazione modulo tastatore" & " - " & Piastra.Matricola_Piastra


            Text1.Visible = False
            Text1.Text = ""

            moRS232 = New Rs232()
            'moRS232.Port = Open_Com
            Scrivi("A piastra spenta, effettuare tutti i collegamenti fra il tastatore e l'apparecchiatura di test" & Chr(13) & "Accertarsi che l'interruttore Power sia in posizione ON, attendere 2 sec e premere un tasto", Color.Blue)
            Attendi_Tasto()

            Scrivi("Connessione Automatica in corso....", Color.Black)

            'If moRS232.IsOpen = False Then moRS232.Open()
connessione:


            Scrivi("Connessione in corso", Color.Black)
            No_Timeout = True
            If Connessione_Automatica() <> 16 Then
                Scrivi(Stringa_di_connessione, Color.Red)
                Attendi_Tasto()
                If Tasto <> 27 Then GoTo connessione
                Stringa_Salvataggio = "Fail | "

                No_Timeout = False
                GoTo fine
            End If

            No_Timeout = False
            Stringa_Salvataggio = " Pass | "
            If Salta_trasf_fw = True Then Stringa_Salvataggio = Stringa_Salvataggio & "NA | NA |" : GoTo Stato_Iniziale

Stato_Iniziale:

            If Stato_Iniziale() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                If Tasto <> 27 Then GoTo Stato_Iniziale
                Stringa_Salvataggio = Stringa_Salvataggio & "Fail | "

                No_Timeout = False
                GoTo fine
            End If

Offset:

            If Offset() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                If Tasto <> 27 Then GoTo offset
                Stringa_Salvataggio = Stringa_Salvataggio & errore_offset
                No_Timeout = False
                GoTo fine
            End If


acquisizione:
            If Acquisizione() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Graph1.Visible = False
                Graph2.Visible = False
                Grid1.Visible = False
                Grid2.Visible = False
                Grid3.Visible = False
                Grid4.Visible = False
                Grid5.Visible = False
                Grid6.Visible = False

                If Tasto <> 27 Then GoTo acquisizione
                Stringa_Salvataggio = Stringa_Salvataggio & Errore_acquisizione

                No_Timeout = False
                GoTo fine
            End If
            Test = True
            Salva_dati(Test)
fine:
        Catch ex As IO.IOException
            MessageBox.Show("Eccezione:" & ex.Message, "Problemi di comunicazione")
            Stringa_Salvataggio = Stringa_Salvataggio & ex.Message & " | "
        Catch ex As Exception
            MessageBox.Show("Eccezione:" & ex.Message, "Errore")
            Stringa_Salvataggio = Stringa_Salvataggio & ex.Message & " | "

        Finally
            If moRS232.IsOpen = True Then Power_OFF()
            If Test = False Then
                Salva_dati(Test)
                Scrivi("Test piastra fallito, premere un tasto per continuare", Color.Red)
                Label2.Visible = True
                Text1.Visible = False
                Grid1.Visible = False
                Grid2.Visible = False
                Grid3.Visible = False
                Grid4.Visible = False
                Grid5.Visible = False
                Grid6.Visible = False
                Graph1.Visible = False
                Graph2.Visible = False
                Attendi_Tasto()
            End If

            Me.Text = "Collaudo e calibrazione modulo tastatore"
            Scrivi("", Color.Black)
            Label2.Visible = False
            Button1.Visible = True
            Button1.Enabled = True
            Button1.Focus()
            Text1.Visible = False
            Grid1.Visible = False
            Grid2.Visible = False
            Grid3.Visible = False
            Grid4.Visible = False
            Grid5.Visible = False
            Grid6.Visible = False
            Graph1.Visible = False
            Graph2.Visible = False
            If moRS232.IsOpen = True Then moRS232.Close()
        End Try
    End Sub

    Private Sub Salva_dati(ByVal test As Boolean)
        Dim Test_Piastra As String
        Dim Data As String = DateTime.Now.Date
        Dim Ora As String = TimeOfDay
        If test = True Then Test_Piastra = " Pass | " Else Test_Piastra = " Fail | "
        Dim matr As String = Piastra.Matricola_Piastra & " | " & Piastra.Nome_operatore & " | " & Data & " | " & Ora & " | " & Test_Piastra
        Dim Intestazione As String '= "Matricola_Piastra | Nome_Operatore | Data | Ora | Test_Piastra | Stato_Result | Trasferimento_FW_Result |Test_Comunicazione_Result | DWL_RIferimento_Result | Foto_In_Result | Encoder_Result |Encoder_Min | Encoder_Med | Encoder_Max | Test_UV_Result | Offset_LeftRight |Offset_Right | Igen_Left | Igen_Right | Min_Left | Med_Left | Max_Left | Min_Right | Med_Right | Max_Right | "

        'Intestazione = Intestazione & "Cis_Result | Stato_Sensori | Front_CIS_Offset_1 |  Front_CIS_Offset_2 | Front_CIS_Offset_3 |  Rear_CIS_Offset_1 | Rear_CIS_Offset_2 | Rear_CIS_Offset_3 | "
        'Intestazione = Intestazione & "Front_CIS_Igen_Ir | Front_CIS_Igen_Gr | Front_CIS_Igen_Rd | Rear_CIS_Igen_Ir | Rear_CIS_Igen_Gr | Rear_CIS_Igen_Rd | "
        'Intestazione = Intestazione & "Front_CIS_Pwm_Ir | Front_CIS_Pwm_Gr | Front_CIS_Pwm_Rd | Rear_CIS_Pwm_Ir | Rear_CIS_Pwm_Gr | Rear_CIS_Pwm_Rd | Verifica_Pattern | "
        'Intestazione = Intestazione & "Test_Tape | Test_Mag | Foto_Trasp_1 | Foto_Trasp_2 | Scrittura_Verifica_Matr | | Dark_Front | GR_Front | IR_Front | RD_Front  | Dark_Rear | GR_Rear | IR_Rear | RD_Rear"




        Dim stringa_dati As String = matr & Stringa_Salvataggio

        Dim myfile = Dir(WorkDir & "Log_CalTast.txt")

        Dim objWriter As New System.IO.StreamWriter(WorkDir & "Log_CalTast.txt", True)

        'If myfile = "" Then objWriter.WriteLine(Intestazione.Replace(" ", ""))

        objWriter.WriteLine(stringa_dati.Replace(" ", ""))
        objWriter.Close()
    End Sub
    Function Stato_Iniziale() As Boolean

        Dim stato As String

        Scrivi("Accensione tastatore, attendere...", Color.Black)

        Power_ON()
        Dim FW_ini As String = Read_ParPiastra("Parametri_riferimento", "fw_release")
        Traspsend("D37C3F", 64, 1000)
        Dim GetVersion As String = stringa_invio(TOGLIE_0(stringa_ricezione(Traspric)))
        Dim FW_tast As String = Mid(GetVersion, 1, 8)
        If FW_ini <> FW_tast Then

            Dim file_bat As String = Read_Set("Configurazione", "Bat_per_download")
            Shell(file_bat, AppWinStyle.NormalFocus, True, )
            Traspsend("D37C3F", 64, 1000)
            GetVersion = stringa_invio(TOGLIE_0(stringa_ricezione(Traspric)))
            FW_tast = Mid(GetVersion, 1, 8)
            If FW_ini <> FW_tast Then
                Stringa_Errore = "Download Firmware Fallito. Verificare cablaggi. Premere un tasto per ripetere, ESC per uscire"
                Stato_Iniziale = False
                Power_OFF()
                Exit Function
            End If
        End If

        Scrivi("Scrittura matricola, attendere...", Color.Black)
        stato = STATUSrequest()
        If stato <> "40" Then
            Stringa_Errore = "Stato modulo diverso da OK, Stato = " & stato & ". Premere un tasto per ripetere, ESC per uscire"
            Stato_Iniziale = False
            Exit Function
        End If

        Call Auto_Answ_ON()

        Dim matr_tast As String = Piastra.Matricola_Piastra.Substring(11, 5)

        matr_tast = matr_tast.PadLeft(8, "0")

        'Dim matr_old As String
        'Call Traspsend("D37C3F", 64, 500)
        'matr_old = stringa_invio(TOGLIE_0(stringa_ricezione(Traspric)))

        SetSerialNumber(matr_tast)

        Dim matr_new As String
        Call Traspsend("D37C3F", 64, 500)
        matr_new = stringa_invio(TOGLIE_0(stringa_ricezione(Traspric)))
        Scrivi("Preset in corso, attendere...", Color.Black)
        stato = Preset()
        If stato <> "40" Then
            Stringa_Errore = "Stato modulo dopo Preset diverso da OK, Stato = " & stato & ". Premere un tasto per ripetere, ESC per uscire"
            Stato_Iniziale = False
            Exit Function
        End If

        Scrivi("Impostazioni tastatore in corso, attendere...", Color.Black)

        SetLineNumcommand()

        SetPrescalercommand()

        Stringa_Salvataggio = "Pass | "

        Stato_Iniziale = True

    End Function
    Function Offset() As Boolean

        Scrivi("Acquisizione Offset in corso, attendere...", Color.Black)

        Call Traspsend("D37C4700000108", 2, 500)

        zero_elettrico = "&h" & TOGLIE_0(stringa_ricezione(Traspric))

        Offset = True

        Dim OFS_low As Integer = zero_elettrico - Read_ParPiastra("Parametri_riferimento", "OFS_low")
        Dim OFS_high As Integer = zero_elettrico + Read_ParPiastra("Parametri_riferimento", "OFS_high")

        Dim Off_set As String = ReadTCKOff()
        Off_set = Mid(Off_set, Off_set.Length / 2 + 1, Off_set.Length / 2)

        cancella_griglia(Grid1)
        Grid1.ColumnCount = 3
        Grid1.RowCount = 8
        Grid1.Item(0, 0).Value = "Ch"
        Grid1.Item(1, 0).Value = "Offset"
        Grid1.Item(2, 0).Value = "Limiti"

        Dim a As Byte
        For a = 1 To 7
            Grid1.Item(2, a).Value = "[" & OFS_low & "/" & OFS_high & "]"
            Grid1.Item(0, a).Value = (a - 1).ToString
        Next

        Dim stringadati As String = zero_elettrico.ToString & " | "
        Dim valore As Integer = 0
        Dim x As Byte = 1
        For a = 1 To 28 Step 4
            valore = ("&h" & Mid(Off_set, a, 4)) / 4
            stringadati = stringadati & valore.ToString & " | "
            Grid1.Item(1, x).Value = valore
            If valore < OFS_low Or valore > OFS_high Then
                Grid1.Item(1, x).Style.ForeColor = Color.Red
                Offset = False
            Else
                Grid1.Item(1, x).Style.ForeColor = Color.Green
            End If
            x = x + 1
        Next
        Grid1.Visible = True
        Ridimensiona_Griglia(Grid1)
        Grid1.Left = (Screen.PrimaryScreen.Bounds.Width - Grid1.Width) / 2

        Attendi_Tasto()
        If Offset = False Then
            Stringa_Errore = "Verifica Offset fallita. Premere un tasto per ripetere, ESC per uscire"
            errore_offset = stringadati
            Exit Function
        End If
        Scrivi("Verifica Offset superata, premere un tasto per continuare", Color.Green)
        Stringa_Salvataggio = Stringa_Salvataggio & stringadati
        Grid1.Visible = False
    End Function
    Function Acquisizione() As Boolean
        Dim stringadati As String = ""
        Dim n_tot As Integer = Read_ParPiastra("Parametri_riferimento", "N_campioni") - 1
        Dim M_min As Integer = Read_ParPiastra("Parametri_riferimento", "m_min")
        Dim M_max As Integer = Read_ParPiastra("Parametri_riferimento", "m_max")
        Dim L0l As Integer = Read_ParPiastra("Parametri_riferimento", "L0l")
        Dim L0h As Integer = Read_ParPiastra("Parametri_riferimento", "L0h")
        Dim L1l As Integer = Read_ParPiastra("Parametri_riferimento", "L1l")
        Dim L1h As Integer = Read_ParPiastra("Parametri_riferimento", "L1h")
        Dim K_neso As Single = Read_ParPiastra("Parametri_riferimento", "K")
        Dim K_BASK As Single = Read_ParPiastra("Parametri_riferimento", "KBASC")
        Dim Offset As Boolean = True

        Scrivi("Acquisizione in corso , attendere...", Color.Black)

        Dim Sn(2) As Integer 'matrice dei punti P3-P5-P7 al netto dell'altezza P1
        Dim P(8) As Integer
        P(0) = Read_ParPiastra("Parametri_riferimento", "P0")
        P(1) = Read_ParPiastra("Parametri_riferimento", "P1")
        P(2) = Read_ParPiastra("Parametri_riferimento", "P2")
        P(3) = Read_ParPiastra("Parametri_riferimento", "P3")
        P(4) = Read_ParPiastra("Parametri_riferimento", "P4")
        P(5) = Read_ParPiastra("Parametri_riferimento", "P5")
        P(6) = Read_ParPiastra("Parametri_riferimento", "P6")
        P(7) = Read_ParPiastra("Parametri_riferimento", "P7")

        Sn(0) = Read_ParPiastra("Parametri_riferimento", "S3")

        Sn(1) = Read_ParPiastra("Parametri_riferimento", "S5")

        Sn(2) = Read_ParPiastra("Parametri_riferimento", "S7")

        EnableNoteIn(True)
        MySleep(100)
        Motor_ON()
        PictureBox1.Visible = True
        Me.Refresh()
        MySleep(2000)
        Application.DoEvents()
        PictureBox1.Visible = True
        Me.Refresh()

        Call Traspsend("F3", 1, 100)

        If stringa_ricezione(Traspric) <> "41" Then
            Acquisizione = False
            Stringa_Errore = "Errore acquisizione dati. Premere un tasto per ripetere, Esc per uscire"
            Errore_acquisizione = ""
            PictureBox1.Visible = False
            Exit Function
        End If

        If attendi_per_t("41", 200, 6000) = False Then
            Acquisizione = False
            Stringa_Errore = "Errore acquisizione dati. Premere un tasto per ripetere, Esc per uscire"
            Errore_acquisizione = ""
            PictureBox1.Visible = False
            Exit Function
        End If

        Scrivi("Riposizionamento in corso, attendere...", Color.Black)
        PictureBox1.Visible = True
        Me.Refresh()
        MySleep(10000)
        PictureBox1.Visible = False
        Me.Refresh()

        Dim Offset_P0 As String = (StartTrack())

        Dim OFS_low As Integer = zero_elettrico - Read_ParPiastra("Parametri_riferimento", "OFS_low")
        Dim OFS_high As Integer = zero_elettrico + Read_ParPiastra("Parametri_riferimento", "OFS_high")
        Dim valore As Integer

        stringadati = stringadati & K_neso.ToString & " | " & Sn(0).ToString & " | " & Sn(1).ToString & " | " & Sn(2).ToString & " | "

        cancella_griglia(Grid1)
        Grid1.ColumnCount = 3
        Grid1.RowCount = 8
        Grid1.Item(0, 0).Value = "Ch"
        Grid1.Item(1, 0).Value = "Offset"
        Grid1.Item(2, 0).Value = "Limiti"
        Grid1.Left = 10
        Grid1.Top = Label1.Top + Label1.Height

        For a = 1 To 7
            Grid1.Item(2, a).Value = "[" & OFS_low & "/" & OFS_high & "]"
            Grid1.Item(0, a).Value = (a - 1).ToString
        Next
        Dim x As Byte = 1
        For a = 1 To 14 Step 2
            valore = "&h" & Mid(Offset_P0, a, 2)
            stringadati = stringadati & valore.ToString & " | "
            Grid1.Item(1, x).Value = valore
            If valore < OFS_low Or valore > OFS_high Then
                Grid1.Item(1, x).Style.ForeColor = Color.Red
                Offset = False
            Else
                Grid1.Item(1, x).Style.ForeColor = Color.Green
            End If
            x = x + 1
        Next

        Ridimensiona_Griglia(Grid1)
        Grid1.Visible = True

        Graph1.Enabled = True
        Dim DATI(,) As Integer = ReadTCKBuf(n_tot, zero_elettrico)

        Graph1.GraphPane = New GraphPane()
        Dim myPane As GraphPane = Graph1.GraphPane

        ' Set the titles and axis labels
        myPane.Title.Text = "Curva non compensata"
        myPane.XAxis.Title.Text = "Punti"
        myPane.YAxis.Title.Text = "Ampiezza"

        ' Make up some data points from the Sine function
        Dim list0 = New PointPairList()
        Dim list1 = New PointPairList()
        Dim list2 = New PointPairList()
        Dim list3 = New PointPairList()
        Dim list4 = New PointPairList()
        Dim list5 = New PointPairList()
        Dim list6 = New PointPairList()

        For a = 0 To 603

            valore = DATI(a, 0)
            list0.Add(a, valore)
            valore = DATI(a, 1)
            list1.Add(a, valore)
            valore = DATI(a, 2)
            list2.Add(a, valore)
            valore = DATI(a, 3)
            list3.Add(a, valore)
            valore = DATI(a, 4)
            list4.Add(a, valore)
            valore = DATI(a, 5)
            list5.Add(a, valore)
            valore = DATI(a, 6)
            list6.Add(a, valore)

        Next


        ' Generate a blue curve with circle symbols, and "My Curve 2" in the legend
        Dim myCurve1 As LineItem = myPane.AddCurve("Ch0", list0, Color.Blue, SymbolType.None)
        Dim mycurve2 As LineItem = myPane.AddCurve("Ch1", list1, Color.Red, SymbolType.None)
        Dim myCurve3 As LineItem = myPane.AddCurve("Ch2", list2, Color.Green, SymbolType.None)
        Dim mycurve4 As LineItem = myPane.AddCurve("Ch3", list3, Color.Aquamarine, SymbolType.None)
        Dim myCurve5 As LineItem = myPane.AddCurve("Ch4", list4, Color.BlueViolet, SymbolType.None)
        Dim mycurve6 As LineItem = myPane.AddCurve("Ch5", list5, Color.Yellow, SymbolType.None)
        Dim myCurve7 As LineItem = myPane.AddCurve("Ch6", list6, Color.Fuchsia, SymbolType.None)
        Graph1.Top = Grid1.Top + Grid1.Height + 10
        Graph1.Left = 10
        Graph1.Visible = True
        Graph1.AxisChange()
        Graph1.Refresh()
        Graph1.Enabled = False
        Graph1.GraphPane.Legend.Position = 12
        Graph1.Refresh()


        Dim pippo = DATI.GetUpperBound(0)
        Dim pluto = DATI.GetUpperBound(1)
        Dim AnPn(7, 6) As Single ' sulle righe i p , sulle colonne i canali
        Dim dato As Single
        For a = 0 To 6
            For b = 0 To 7
                dato = 0
                For c = -2 To 2
                    dato = dato + DATI(P(b) + 2 + c, a)
                Next
                dato = dato / 5
                AnPn(b, a) = dato
                stringadati = stringadati & dato.ToString & " | "
            Next
        Next

        'Dim k As Integer = 0
        'Dim Sn(2, 6) As Byte 'matrice dei punti P3-P5-P7 al netto dell'altezza P1
        'For a = 0 To 6
        '    For b = 2 To 6 Step 2

        '        Sn(k, a) = AnPn(b, a) - AnPn(0, a)
        '        k = k + 1
        '    Next
        '    k = 0
        'Next



        Dim Mn(2, 6) As Single 'matrice degli M

        For a = 0 To 6
            Mn(0, a) = ((Sn(0) / K_neso) * 256) / AnPn(3, a)
            Mn(1, a) = (((Sn(1) - Sn(0)) / K_neso) * 256) / (AnPn(5, a) - AnPn(3, a))
            Mn(2, a) = (((Sn(2) - Sn(1)) / K_neso) * 256) / (AnPn(7, a) - AnPn(5, a))
            stringadati = stringadati & Round(Mn(0, a)).ToString & " | " & Round(Mn(1, a)).ToString & " | " & Round(Mn(2, a)).ToString & " | "
        Next

        Dim Qn(2, 6) As Single 'matrice degli Q

        For a = 0 To 6
            Qn(0, a) = 0
            Qn(1, a) = (Sn(0) / K_neso - ((Mn(1, a) * AnPn(3, a)) / 256)) * 4
            Qn(2, a) = (Sn(1) / K_neso - ((Mn(2, a) * AnPn(5, a)) / 256)) * 4
            stringadati = stringadati & Qn(0, a).ToString & " | " & Round(Qn(1, a)).ToString & " | " & Round(Qn(2, a)).ToString & " | "

        Next

        Grid2.Left = Grid1.Left + Grid1.Width + 5
        Grid2.Top = Grid1.Top
        Grid2.ColumnCount = 5
        Grid2.RowCount = 8
        Grid2.Item(0, 0).Value = "Ch"
        Grid2.Item(1, 0).Value = "m_P3"
        Grid2.Item(2, 0).Value = "m_P5"
        Grid2.Item(3, 0).Value = "m_P7"
        Grid2.Item(4, 0).Value = "Limiti"
        For a = 1 To 7
            Grid2.Item(4, a).Value = "[" & M_min & "/" & M_max & "]"
            Grid2.Item(0, a).Value = (a - 1).ToString
        Next

        Dim test_m As Boolean = True
        For a = 0 To 2
            For b = 0 To 6
                Grid2.Item(a + 1, b + 1).Value = Mn(a, b)
                If Mn(a, b) < M_min Or Mn(a, b) > M_max Then
                    Grid2.Item(a + 1, b + 1).Style.ForeColor = Color.Red
                    test_m = False
                Else
                    Grid2.Item(a + 1, b + 1).Style.ForeColor = Color.Green
                End If
            Next
        Next
        Grid2.Visible = True
        Ridimensiona_Griglia(Grid2)

        Grid3.Left = Grid2.Left + Grid2.Width + 5
        Grid3.Top = Grid1.Top
        Grid3.ColumnCount = 3
        Grid3.RowCount = 8
        Grid3.Item(0, 0).Value = "Ch"
        Grid3.Item(1, 0).Value = "AnP0"
        Grid3.Item(2, 0).Value = "Limiti"

        For a = 1 To 7
            Grid3.Item(2, a).Value = "[" & L0l & "/" & L0h & "]"
            Grid3.Item(0, a).Value = (a - 1).ToString
        Next

        Dim test_a_1 As Boolean = True
        For a = 0 To 6
            Grid3.Item(1, a + 1).Value = AnPn(0, a)
            If AnPn(0, a) < L0l Or AnPn(0, a) > L0h Then
                Grid3.Item(1, a + 1).Style.ForeColor = Color.Red
                test_a_1 = False
            Else
                Grid3.Item(1, a + 1).Style.ForeColor = Color.Green
            End If
        Next

        Grid3.Visible = True
        Ridimensiona_Griglia(Grid3)

        Grid4.Left = Grid3.Left + Grid3.Width + 5
        Grid4.Top = Grid1.Top
        Grid4.ColumnCount = 3
        Grid4.RowCount = 8
        Grid4.Item(0, 0).Value = "Ch"
        Grid4.Item(1, 0).Value = "AnP1-AnP2"
        Grid4.Item(2, 0).Value = "Limiti"

        For a = 1 To 7
            Grid4.Item(2, a).Value = "[" & L1l & "/" & L1h & "]"
            Grid4.Item(0, a).Value = (a - 1).ToString
        Next

        Dim test_a_2 As Boolean = True
        Dim VAL As Single
        For a = 0 To 6

            VAL = (AnPn(1, a) - AnPn(2, a)) * Mn(0, a) / 256
            Grid4.Item(1, a + 1).Value = VAL
            If VAL < L1l Or VAL > L1h Then
                Grid4.Item(1, a + 1).Style.ForeColor = Color.Red
                test_a_2 = False
            Else
                Grid4.Item(1, a + 1).Style.ForeColor = Color.Green
            End If
        Next
        Grid4.Visible = True
        Ridimensiona_Griglia(Grid4)
        Grid5.ColumnCount = 3
        Grid5.RowCount = 8
        Grid6.ColumnCount = 3
        Grid6.RowCount = 8
        Dim test_Basc As Boolean = True

        '(1-Kbasc)*(AnP5+AnP3)/2 < AnP4 < (1+Kbasc)*(AnP5+AnP3)/2  
        '(1-Kbasc)*(AnP5+AnP7)/2 < AnP6 < (1+Kbasc)*(AnP5+AnP7)/2  

        For a = 0 To 6
            Dim Minore As Single = (AnPn(5, a) + AnPn(3, a)) / 2 * (1 - K_BASK / 100)
            Dim Maggiore As Single = (AnPn(5, a) + AnPn(3, a)) / 2 * (1 + K_BASK / 100)
            Grid5.Item(2, a + 1).Value = "[" & Minore & "/" & Maggiore & "]"
            Grid5.Item(0, a + 1).Value = (a).ToString
            Grid5.Item(1, a + 1).Value = AnPn(4, a)
            If AnPn(4, a) < Minore Or AnPn(4, a) > Maggiore Then
                test_Basc = False
                Grid5.Item(1, a + 1).Style.ForeColor = Color.Red
            Else
                Grid5.Item(1, a + 1).Style.ForeColor = Color.Green
            End If

            Minore = (AnPn(5, a) + AnPn(7, a)) / 2 * (1 - K_BASK / 100)
            Maggiore = (AnPn(5, a) + AnPn(7, a)) / 2 * (1 + K_BASK / 100)
            Grid6.Item(2, a + 1).Value = "[" & Minore & "/" & Maggiore & "]"
            Grid6.Item(0, a + 1).Value = (a).ToString
            Grid6.Item(1, a + 1).Value = AnPn(6, a)
            If AnPn(6, a) < Minore Or AnPn(6, a) > Maggiore Then
                test_Basc = False
                Grid6.Item(1, a + 1).Style.ForeColor = Color.Red
            Else
                Grid6.Item(1, a + 1).Style.ForeColor = Color.Green
            End If
        Next

        Grid5.Left = Grid4.Left + Grid4.Width + 5
        Grid5.Top = Grid1.Top

        Grid5.Item(0, 0).Value = "Ch"
        Grid5.Item(1, 0).Value = "AnP4"
        Grid5.Item(2, 0).Value = "Limiti"
        Grid5.Visible = True
        Ridimensiona_Griglia(Grid5)

        Grid6.Left = Grid5.Left + Grid5.Width + 5
        Grid6.Top = Grid1.Top

        Grid6.Item(0, 0).Value = "Ch"
        Grid6.Item(1, 0).Value = "AnP6"
        Grid6.Item(2, 0).Value = "Limiti"
        Grid6.Visible = True
        Ridimensiona_Griglia(Grid6)

        If Offset = False Or test_m = False Or test_a_1 = False Or test_a_2 = False Or test_Basc = False Then
            Acquisizione = False
            Stringa_Errore = "Fallita calibrazione tastatore, premere un tasto per ripetere, Esc per uscire"
            Errore_acquisizione = stringadati
            Exit Function
        End If



        Dim Stringa_Calibrazione As String = ""
        Dim aaa As String = ""
        Dim mmm As String = ""
        Dim qqq As String = ""
        Dim a3, a5, a7 As Integer
        Dim q3, q5, q7 As String
        For a = 0 To 6

            a3 = AnPn(3, a) * 4
            a5 = AnPn(5, a) * 4
            a7 = AnPn(7, a) * 4
            aaa = Mette_4_0(Hex(a3)) & Mette_4_0(Hex(a5)) & Mette_4_0(Hex(a7))
            mmm = Mette_4_0(Hex(Round(Mn(0, a)))) & Mette_4_0(Hex(Round(Mn(1, a)))) & Mette_4_0(Hex(Round(Mn(2, a))))
            If Qn(0, a) < 0 Then
                q3 = (Round(Qn(0, a)) Xor &HFFFFFFFF) + 1
            Else
                q3 = Round(Qn(0, a))
            End If
            If Qn(1, a) < 0 Then
                Dim QQ As Int16 = 0
                QQ = Round(Qn(1, a))
                q5 = Math.Abs((QQ Xor &HFFFF) + 1).ToString
            Else
                q5 = Round(Qn(1, a))
            End If
            If Qn(2, a) < 0 Then
                Dim QQ As Int16 = 0
                QQ = Round(Qn(2, a))
                q7 = Math.Abs((QQ Xor &HFFFF) + 1).ToString
            Else
                q7 = Round(Qn(2, a))
            End If
            'Dim GIGGI As Int16 = dec_hex(q5)
            qqq = Mette_4_0(Hex(q3)) & Mette_4_0(Hex(q5)) & Mette_4_0(Hex(q7))

            Dim stringa_dati As String = METTE_0(aaa) & METTE_0(mmm) & METTE_0(qqq)
            Dim Salta_scrittura_M_Q As Byte = Read_Set("Debug", "Salta_scrittura_M_Q")
            If Salta_scrittura_M_Q = 0 Then
                Call Traspsend("D37C420" & a.ToString & stringa_dati, 0, 0)
                MySleep(100)
            End If
        Next

        Call Traspsend("D37C35", 0, 0)
        MySleep(500)
        Scrivi("Calibrazione Tastatore superata, premere un tasto per continuare", Color.Green)
        Stringa_Salvataggio = stringadati
        Attendi_Tasto()
        Dim Salta_seconda_passata As Byte = Read_Set("Debug", "Salta_seconda_passata")
        If Salta_seconda_passata = 0 Then


            EnableNoteIn(True)
            MySleep(100)
            Motor_ON()
            PictureBox1.Visible = True
            Me.Refresh()
            Application.DoEvents()
            MySleep(2000)
            PictureBox1.Visible = True
            Me.Refresh()
            Application.DoEvents()

            Call Traspsend("F3", 1, 100)

            If stringa_ricezione(Traspric) <> "41" Then
                Acquisizione = False
                Stringa_Errore = "Errore acquisizione dati. Premere un tasto per ripetere, Esc per uscire"
                Errore_acquisizione = ""
                PictureBox1.Visible = False
                Exit Function
            End If

            If attendi_per_t("41", 200, 6000) = False Then
                Acquisizione = False
                Stringa_Errore = "Errore acquisizione dati. Premere un tasto per ripetere, Esc per uscire"
                Errore_acquisizione = ""
                PictureBox1.Visible = False
                Exit Function
            End If

            'Scrivi("Riposizionamento in corso, attendere...", Color.Black)
            PictureBox1.Visible = True
            Me.Refresh()
            Application.DoEvents()
            MySleep(10000)

            PictureBox1.Visible = False
            Me.Refresh()
            DATI = ReadTCKBuf(n_tot, zero_elettrico)

            Graph2.GraphPane = New GraphPane()
            Dim myPane2 As GraphPane = Graph2.GraphPane

            ' Set the titles and axis labels
            myPane2.Title.Text = "Curva compensata"
            myPane2.XAxis.Title.Text = "Punti"
            myPane2.YAxis.Title.Text = "Ampiezza"

            ' Make up some data points from the Sine function
            list0.Clear()
            list1.Clear()
            list2.Clear()
            list3.Clear()
            list4.Clear()
            list5.Clear()
            list6.Clear()
            Dim kkkk As Byte
            For a = 0 To 603
                If a < 225 Then
                    kkkk = 0
                End If
                If a >= 225 And a < 375 Then
                    kkkk = 1
                End If
                If a >= 375 Then
                    kkkk = 2
                End If
                valore = DATI(a, 0) '* round(Mn(kkkk, 0)) / 256 + round(Qn(kkkk, 0))
                list0.Add(a, valore)
                valore = DATI(a, 1) '* round(Mn(kkkk, 1)) / 256 + round(Qn(kkkk, 1))
                list1.Add(a, valore)
                valore = DATI(a, 2) '* Mn(kkkk, 2) / 256 + Qn(kkkk, 2)
                list2.Add(a, valore)
                valore = DATI(a, 3) '* Mn(kkkk, 3) / 256 + Qn(kkkk, 3)
                list3.Add(a, valore)
                valore = DATI(a, 4) '* Mn(kkkk, 4) / 256 + Qn(kkkk, 4)
                list4.Add(a, valore)
                valore = DATI(a, 5) '* Mn(kkkk, 5) / 256 + Qn(kkkk, 5)
                list5.Add(a, valore)
                valore = DATI(a, 6) '* Mn(kkkk, 6) / 256 + Qn(kkkk, 6)
                list6.Add(a, valore)

            Next


            ' Generate a blue curve with circle symbols, and "My Curve 2" in the legend
            Dim myCurve11 As LineItem = myPane2.AddCurve("Ch0", list0, Color.Blue, SymbolType.None)
            Dim myCurve12 As LineItem = myPane2.AddCurve("Ch1", list1, Color.Red, SymbolType.None)
            Dim myCurve13 As LineItem = myPane2.AddCurve("Ch2", list2, Color.Green, SymbolType.None)
            Dim myCurve14 As LineItem = myPane2.AddCurve("Ch3", list3, Color.Aquamarine, SymbolType.None)
            Dim myCurve15 As LineItem = myPane2.AddCurve("Ch4", list4, Color.BlueViolet, SymbolType.None)
            Dim myCurve16 As LineItem = myPane2.AddCurve("Ch5", list5, Color.Yellow, SymbolType.None)
            Dim myCurve17 As LineItem = myPane2.AddCurve("Ch6", list6, Color.Fuchsia, SymbolType.None)
            Graph2.Top = Grid1.Top + Grid1.Height + 10
            Graph2.Left = Graph1.Left + Graph1.Width + 5
            Graph2.Visible = True
            Graph2.AxisChange()
            Graph2.GraphPane.Legend.Position = 12

            Graph2.Refresh()
            Attendi_Tasto()

        End If
        Acquisizione = True
        Graph1.Visible = False
        Graph2.Visible = False
        Grid1.Visible = False
        Grid2.Visible = False
        Grid3.Visible = False
        Grid4.Visible = False
        Grid5.Visible = False
        Grid6.Visible = False
        Button1.Visible = True

    End Function

    Sub Motor_ON()
        MySleep(20)
        Call Traspsend("D902", 0, 0)
        MySleep(200)
    End Sub
    Sub Power_OFF()
        MySleep(20)
        Call Traspsend("D900", 0, 0)
        MySleep(200)
    End Sub
    Sub Power_ON()
        MySleep(20)
        Call Traspsend("D901", 0, 0)
        MySleep(2000)
    End Sub

    Function STATUSrequest() As String

        'Rs232(cmd)0xD3 0x7C 0x54
        'Parms()--
        'Answer()0d 0d
        'T/Out max 5MS()
        Call Traspsend("D37C54", 2, 3000)
        STATUSrequest = TOGLIE_0(stringa_ricezione(Traspric))

    End Function
    Function Preset() As String

        'Rs232(cmd)0xD3 0x7C 0x34
        'Parms()
        'Answer()0d 0d
        'T/Out max 3sec()

        Call Traspsend("D37C34", 2, 3000)
        Preset = TOGLIE_0(stringa_ricezione(Traspric))
    End Function

    Function SetSerialNumber(ByVal sn As String) As Boolean

        'Rs232(cmd)0xD3 0x7C 0x4C
        'Parms()8ASCII(chars)
        'Answer()
        'T/Out max 100:    MS()
        'Permette di impostare il serial number del modulo
        sn = stringa_ricezione(sn)
        Call Traspsend("D31400" & sn, 0, 0)
        Dim cod_rev As String = Piastra.Matricola_Piastra.Substring(1, 5) & "-" & Piastra.Matricola_Piastra.Substring(6, 2)
        Dim hw_lev As String = Read_hwlev(cod_rev, "hw_level").PadLeft(3, "0")
        For a = 0 To 2
            Dim adr = dec_hex(&H78 + a)
            Dim dato = stringa_ricezione(hw_lev.Substring(a, 1))
            scrivi_spt("00", adr, dato)
        Next
        cod_rev.PadLeft(3, "0")
        'Call Traspsend("D37C3F", 64, 1000)
        'Dim kkk As String = stringa_invio(TOGLIE_0(stringa_ricezione((Traspric))))
        SetSerialNumber = True
        Call MySleep(200)
    End Function
    Function scrivi_spt(ByVal page As String, ByVal Addr As String, ByVal dato As String)
        'Scrittura della spt del sensore di tape: D3 7C 53 0p 0p 0a 0a 0d 0d
        Dim str As String = METTE_0(page & Addr & dato)
        Traspsend("D37C53" & str, 0, 100)

    End Function
    Function StartTrack() As String
        'Rs232(cmd)0xD3 0x7C 0x21
        'Parms()
        'Answer()16 x [0d 0d]
        'T/Out max 100MS()

        Call Traspsend("D37C33", 32, 3000)
        StartTrack = TOGLIE_0(stringa_ricezione(Traspric))

    End Function

    Function SetTCKcfx(ByVal ch As Byte, ByVal a() As String, ByVal m() As String, ByVal q() As String)

        '(cmd)0xD3 0x7C 0x42
        'Parms()ch, 3x[0a 0a0a 0a], 3x[0m 0m0m 0m],3 x [0q 0q0q 0q]
        'Answer()
        'T/Out max 100ms()

        'Esegue la scrittura dei coefficienti di compensazione per un dato elemento dell’array tastatore.
        Dim aaaa = METTE_0(a(0)) & METTE_0(a(1)) & METTE_0(a(2)) & METTE_0(a(3))
        Dim mmmm = METTE_0(m(0)) & METTE_0(m(1)) & METTE_0(m(2)) & METTE_0(m(3))
        Dim qqqq = METTE_0(q(0)) & METTE_0(q(1)) & METTE_0(q(2)) & METTE_0(q(3))

        Call Traspsend("D37C42" & "0" & Hex(ch) & aaaa & mmmm & qqqq, 0, 100)
        SetTCKcfx = True
    End Function

    Function EnableNoteIn(ByVal Switch As Boolean)

        'RS232(cmd)0xD3 0x07 0x04
        'Parms(Switch())
        'Answer()0d 0d
        'T/Out max 5ms()

        'Comando usato per abilitare o disabilitare la funzione di fotosensore di ingresso banconota (impostazione non residente che va persa allo spegnimento).
        'Switch = 00 (OFF) il fotosensore di ingresso è disabilitato (non rileva il passaggio luce/buio);
        'Switch = 01 (ON) il fotosensore di ingresso è abilitato (condizione di default all’accensione).
        'La risposta dd rappresenta lo StatusCode
        Dim aa As String

        If Switch = True Then
            aa = "01"
        Else
            aa = "00"
        End If

        Call Traspsend("D30704" & aa, 1, 1000)

        EnableNoteIn = Traspric

    End Function

    Function ReadTCKBuf(ByVal n_tot As Integer, ByVal zero_elettrico As Byte) As Integer(,)

        Dim STRINGA = USBB("20", 1, 12, False)
        STRINGA = USBB("6A", 1, 65536, False)
        Dim DATI(n_tot, 15) As Integer
        Dim zer_el As Integer = zero_elettrico
        For a = 0 To n_tot
            For b = 0 To 15
                DATI(a, b) = zer_el - STRINGA(b + (a * 16))
            Next
        Next

        Return DATI
    End Function

    Function ReadTCKOff() As String

        'RS232(cmd) 0xD3 0x7C 0x55
        'Parms(0) : x12()
        'Answer()16 x [0d 0d 0d 0d]+ 16 x [0d 0d 0d 0d]
        'T/Out max 5ms()

        'Le prime 16 word contengono i valori del DAC, corrispondenti al punto di riposo. Il secondo gruppo di 16 word rappresenta il valore raggiunto come punto di riposo.

        Call Traspsend("D37C5512", 128, 1000)

        ReadTCKOff = TOGLIE_0(stringa_ricezione(Traspric))

    End Function
    Sub SetLineNumcommand()
        Dim aa As String = Read_ParPiastra("Parametri_riferimento", "N_campioni")
        'RS232(cmd)0xD3 0x7C 0x53 
        'Parms()0x00 0x01 0x05 0x00 0x0d 0x0d
        'Answer()--
        'T/Out max  5ms()
        Dim bb As String = METTE_0(Mid(Mette_4_0(Hex(aa)), 3, 2))

        Dim cc As String = METTE_0(Mid(Mette_4_0(Hex(aa)), 1, 2))
        Call Traspsend("D37C53" & "00010501" & (bb), 0, 0)
        Call MySleep(50)
        Call Traspsend("D37C53" & "00010500" & (cc), 0, 0)
        Call MySleep(50)
    End Sub

    Sub SetPrescalercommand()
        Dim aa As String = Mette_2_0(Hex(Read_ParPiastra("Parametri_riferimento", "Prescaler")))
        'RS232(cmd)0xD3 0x7C 0x53 
        'Parms()0x00 0x00 0x00 0x03 0x0d 0x0d
        'Answer()--
        'T/Out max 5ms()
        'Scrive il numero di impulsi di clock (2KHz) tra due successive acquisizioni.  Default 20d (equivale  ad una frequenza di campionamento pari a 100Hz).

        Call Traspsend("D37C53" & "00000003" & METTE_0(aa), 0, 0)
        MySleep(50)
    End Sub

    Private Sub Debug_test_control()

        If Debug_pausa > 0 Then
            MySleep(Debug_pausa * 1000)
        End If

        If Debug_test = True Then
            Attendi_Tasto()
        End If
    End Sub

    Public Sub Ridimensiona_Griglia(ByVal Griglia As Object)

        Dim Larghezza As Integer
        Dim Altezza As Integer

        For a = 1 To Griglia.Columns.Count
            Griglia.AutoResizeColumn(a - 1)
        Next

        Larghezza = 3
        For a = 1 To Griglia.Columns.Count
            Larghezza = Larghezza + Griglia.Columns(a - 1).Width
        Next
        Griglia.Width = Larghezza

        Altezza = 3
        For a = 1 To Griglia.Rows.Count
            Altezza = Altezza + Griglia.Rows(a - 1).Height
        Next

        Griglia.Height = Altezza

        Application.DoEvents()
    End Sub
    Public Sub cancella_griglia(ByVal Griglia As Object)
        For a = 0 To Griglia.Rows.Count - 1
            For b = 0 To Griglia.Columns.Count - 1
                Griglia.Item(b, a).Style.ForeColor = Color.Black
                Griglia.Item(b, a).Value = ""
            Next
        Next
    End Sub

    Public Sub Scrivi(ByVal Stringa As String, ByVal col As Color)
        Label1.ForeColor = col
        Label1.Text = Stringa
        Application.DoEvents()
    End Sub


#Region "UI update routine"
#End Region



    Public Sub Mem_Dump(ByVal DevId As Byte, ByVal BlockAdr As String, ByVal BlockSize As String)

        BlockAdr = METTE_0(BlockAdr)
        Dim BlockSizes As String = METTE_0(BlockSize)
        Dim aa As String
        aa = "D3330" & Hex(DevId) & BlockAdr & BlockSizes
        Traspsend(aa, (CLng("&h" & BlockSize) * 2), 15000)
    End Sub

    Public Sub Mem_Write(ByVal DevId As Byte, ByVal BlockAdr As String, ByVal BlockSize As String, ByVal DataStream As String)

        BlockAdr = METTE_0(BlockAdr)
        BlockSize = METTE_0(BlockSize)
        DataStream = METTE_0(DataStream)
        Dim aa As String
        aa = "D3331" & DevId.ToString & BlockAdr & BlockSize & DataStream

        Traspsend(aa, 0, 100)
    End Sub




    Function Cambio_Velocita_Seriale(ByVal velocita As Integer, ByVal invio As Boolean)
        Dim caga As String
        Traspric = ""
        'attendi_per("41")

        If invio = True Then
            If velocita = 4 Then
                Call Traspsend("D37700", 0, 0)
            Else
                Call Traspsend("D3470" + Trim(Str(velocita)), 0, 0)
            End If

        End If
        Call MySleep(1000)
        Select Case velocita
            Case 0
                moRS232.BaudRate = 9600

            Case 1
                moRS232.BaudRate = 19200

            Case 2
                moRS232.BaudRate = 38400

            Case 3
                moRS232.BaudRate = 57600

            Case 4
                moRS232.BaudRate = 115200
        End Select
        'If invio = True Then
        '    If velocita <> 4 Then
        '        Call ricezione("", 1, 6000)
        '    End If
        'End If
        Call MySleep(1000)
        moRS232.Close()
        moRS232.Open()
        If moRS232.InBufferCount > 0 Then
            moRS232.Read(moRS232.InBufferCount)
            caga = moRS232.InputStreamString
        End If
        caga = moRS232.BaudRate
        attendi_per_t("41", 500, 2000)

        If Traspric <> "" Then Cambio_Velocita_Seriale = True Else Cambio_Velocita_Seriale = False

    End Function
    Public Sub Ext_Trig()
        Dim aa As String
        Call Traspsend("D355", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Call Traspsend("D35401", 1, 3000)
        End If

    End Sub
    Public Sub Int_Trig()
        Dim aa As String
        Call Traspsend("D355", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call Traspsend("D35400", 1, 3000)
        End If

    End Sub
    Public Sub Auto_Answ_ON()
        Dim aa As String
        Call Traspsend("D353", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Call Traspsend("D35201", 1, 3000)
        End If
    End Sub

    Public Sub Auto_Answ_OFF()
        Dim aa As String
        Call Traspsend("D353", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call Traspsend("D35200", 0, 0)
            attendi_per("41")
        End If
    End Sub

    Public Sub Dbl_On()
        Dim aa As String
        Call Traspsend("D349", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Call Traspsend("D34801", 1, 3000)
        End If
    End Sub

    Public Sub Dbl_Off()
        Dim aa As String
        Call Traspsend("D349", 1, 5)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call Traspsend("D34800", 1, 3000)
        End If
    End Sub

    Public Sub Reboot(Optional ByVal cambiare_velocità As Boolean = False, Optional ByVal velocità_seriale As Integer = 0)
        'Dim usb_ric As String
        Call Traspsend("D37D", 1, 5000)
        MySleep(2500)
        If cambiare_velocità = True Then
            Cambio_Velocita_Seriale(velocità_seriale, False)
        End If

        attendi_per("41")
        Call MySleep(200)
        Call Traspsend("D31300", 0, 0)
        Call MySleep(200)
        Call Traspsend("F3", 1, 1000)
        'For a = 1 To 3
        '    usb_ric = USBB("20", 2, 13, False)
        '    If Mid$(usb_ric, 1, 12) = "123456 prova" Then
        '        Exit Sub
        '    End If
        '    Call MySleep(2000)
        'Next

    End Sub

    Private Sub ProgressBar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgressBar1.Click

    End Sub

    Private Sub Text1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyDown
        Tasto = e.KeyCode
    End Sub


    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim DATI(,) As Integer = ReadTCKBuf(605, zero_elettrico)
        Dim r As New Random(Round(Date.Now.Ticks And Integer.MaxValue))
        'Dim b As New Bitmap(1000, 400)
        'Dim g As Graphics = Graphics.FromImage(b)


        'Dim pn As New Pen(Color.Blue)
        'Dim pt1 As New Point(30, 30)
        'Dim pt2 As New Point(110, 100)



        'b.Dispose()
        'Dim b As New Bitmap(1000, 800)


        'For y = 0 To 64

        '    b.SetPixel(20, y, Color.Red)

        'Next

        'Dim p As New Panel



        Dim valore As Integer = r.Next(0, 5)

        For a = 0 To 604
            For c = 0 To 6

                DATI(a, c) = (10 + Int(a / 10) + r.Next(5) + c * 1.3)
            Next
        Next
        'Graf1.Visible = True
        ''Dim pic As Bitmap
        'pn = New Pen(Color.Black)
        'g.DrawLine(pn, 10, b.Height - 20, 700, b.Height - 20)
        'g.DrawLine(pn, 20, b.Height - 10, 20, 20)

        'pn = New Pen(Color.Blue)
        'For a = 0 To 603

        '    valore = b.Height - DATI(a, 1) - 20
        '    valore1 = b.Height - DATI(a + 1, 1) - 20
        '    g.DrawLine(pn, a + 20, valore, a + 1 + 20, valore1)
        '    b.SetPixel(a + 20, valore, Color.Red)
        'Next
        'g.DrawImage(b, 0, 0)

        'g.Dispose()
        'Graf1.Visible = True
        'Graf1.BringToFront()
        'Graf1.Image = b 'Graf1.Image = b
        ''Graf1.Visible = True
        Graph1.GraphPane = New GraphPane()
        Dim myPane As GraphPane = Graph1.GraphPane

        ' Set the titles and axis labels
        myPane.Title.Text = "My Test Date Graph"
        myPane.XAxis.Title.Text = "X Value"
        myPane.YAxis.Title.Text = "My Y Axis"

        ' Make up some data points from the Sine function
        Dim list0 = New PointPairList()
        Dim list1 = New PointPairList()
        Dim list2 = New PointPairList()
        Dim list3 = New PointPairList()
        Dim list4 = New PointPairList()
        Dim list5 = New PointPairList()
        Dim list6 = New PointPairList()

        For a = 0 To 603

            valore = DATI(a, 0)
            list0.Add(a, valore)
            valore = DATI(a, 1)
            list1.Add(a, valore + 10)
            valore = DATI(a, 2)
            list2.Add(a, valore + 20)
            valore = DATI(a, 3)
            list3.Add(a, valore + 30)
            valore = DATI(a, 4)
            list4.Add(a, valore + 40)
            valore = DATI(a, 5)
            list5.Add(a, valore + 50)
            valore = DATI(a, 6)
            list6.Add(a, valore + 60)

        Next


        ' Generate a blue curve with circle symbols, and "My Curve 2" in the legend
        Dim myCurve1 As LineItem = myPane.AddCurve("Ch0", list0, Color.Blue, SymbolType.None)
        Dim mycurve2 As LineItem = myPane.AddCurve("Ch1", list1, Color.Red, SymbolType.None)
        Dim myCurve3 As LineItem = myPane.AddCurve("Ch2", list2, Color.Green, SymbolType.None)
        Dim mycurve4 As LineItem = myPane.AddCurve("Ch3", list3, Color.Aquamarine, SymbolType.None)
        Dim myCurve5 As LineItem = myPane.AddCurve("Ch4", list4, Color.BlueViolet, SymbolType.None)
        Dim mycurve6 As LineItem = myPane.AddCurve("Ch5", list5, Color.Yellow, SymbolType.None)
        Dim myCurve7 As LineItem = myPane.AddCurve("Ch6", list6, Color.Fuchsia, SymbolType.None)
        Graph1.Visible = True
        Graph1.AxisChange()
        Graph1.Refresh()

        ' Fill the area under the curve with a white-red gradient at 45 degrees
        'myCurve.Line.Fill = New Fill(Color.White, Color.Red, 45.0F)
        ' Make the symbols opaque by filling them with white
        'myCurve.Symbol.Fill = New Fill(Color.White)

        ' Fill the axis background with a color gradient
        'myPane.Chart.Fill = New Fill(Color.White, Color.LightGoldenrodYellow, 45.0F)

        ' Fill the pane background with a color gradient
        'myPane.Fill = New Fill(Color.White, Color.FromArgb(220, 220, 255), 45.0F)

        ' Calculate the Axis Scale Ranges


    End Sub

    Private Sub Graf1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Graph1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Graph1.KeyDown
        Tasto = e.KeyCode
    End Sub




    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Call CreateGraph(Graph1)
    End Sub

    Private Sub CreateGraph(ByVal zgc As ZedGraphControl)
        Dim myPane As GraphPane = zgc.GraphPane

        ' Set the titles and axis labels
        myPane.Title.Text = "My Test Date Graph"
        myPane.XAxis.Title.Text = "X Value"
        myPane.YAxis.Title.Text = "My Y Axis"

        ' Make up some data points from the Sine function
        Dim list0 = New PointPairList()
        Dim list1 = New PointPairList()
        Dim x As Double, y As Double
        For x = 0 To 36
            y = Math.Sin(x * Math.PI / 15.0)
            list0.Add(x, y)
            y = Math.Cos(x * Math.PI / 15.0)
            list1.Add(x, y)

        Next x

        ' Generate a blue curve with circle symbols, and "My Curve 2" in the legend
        Dim myCurve1 As LineItem = myPane.AddCurve("seno", list0, Color.Blue, SymbolType.None)
        Dim mycurve2 As LineItem = myPane.AddCurve("coseno", list1, Color.Red, SymbolType.None)
        ' Fill the area under the curve with a white-red gradient at 45 degrees
        'myCurve.Line.Fill = New Fill(Color.White, Color.Red, 45.0F)
        ' Make the symbols opaque by filling them with white
        'myCurve.Symbol.Fill = New Fill(Color.White)

        ' Fill the axis background with a color gradient
        'myPane.Chart.Fill = New Fill(Color.White, Color.LightGoldenrodYellow, 45.0F)

        ' Fill the pane background with a color gradient
        'myPane.Fill = New Fill(Color.White, Color.FromArgb(220, 220, 255), 45.0F)

        ' Calculate the Axis Scale Ranges
        zgc.AxisChange()
        zgc.Refresh()
    End Sub

    Private Sub Graph2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Graph2.KeyDown
        Tasto = e.KeyCode
    End Sub



    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub
End Class
